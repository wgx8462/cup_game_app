import 'package:cup_game_app/compornents/cup_choice_item.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  bool _isStart = false; // 시작했냐?
  num pandon = 100;
  List<num> result = [0, 100, 200];

  void _startGame() {
    setState(() {
      _isStart = true;
      result.shuffle();
      print(result);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: const Text(
          '야바위',
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: _bilBody(context),
    );
  }

  Widget _bilBody(BuildContext context) {
    if (_isStart) {
      return SingleChildScrollView(
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.fromLTRB(0, 200, 0, 0),
                child: Column(
                  children: [
                    Container(
                      child: const Text(
                          '동전묶음 : 200원 | 동전한개 100원 | 3등 꽝',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ],
                )
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CupChoiceItem(pandon: pandon, currentMoney: result[0]),
                  CupChoiceItem(pandon: pandon, currentMoney: result[1]),
                  CupChoiceItem(pandon: pandon, currentMoney: result[2]),
                ],
              ),
            ),
            Container(
              child: OutlinedButton(
                onPressed: () {
                  setState(() {
                    _isStart = false;
                  });
                },
                child: const Text('종료'),
              ),
            ),
          ],
        ),
      );
    } else {
      return SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 290, 0, 0),
              child: const Text(
                  '3인용 야바위',
                style: TextStyle(
                  fontSize: 25,
                ),
              ),
            ),
            Container(
              child: const Text(
                '참가비 100원',
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Center(
                child: OutlinedButton(
                  onPressed: () {
                    _startGame();
                  },
                  child: const Text('시작'),
                ),
              ),
            ),
          ],),
      );
    }
  }
}
