import 'package:flutter/material.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem(
      {super.key, required this.pandon, required this.currentMoney});

  final num pandon; // 100
  final num currentMoney; // 0

  @override
  State<CupChoiceItem> createState() => _CupChoiceItemState();
}

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc = 'assets/cup.png';

  void _alsdkfj() {
    if (!_isOpen) {
      setState(() {
        _isOpen = true;
      });
    }
  }

  void _calculateImgSrc() {
    // 상자오픈 했을때......
    // 판돈이 현재돈보다 적을때 : Exc.png
    // 판돈이 현재돈과 같을때 : one.Png
    // 판돈이 현재돈 보다 많을때 : two.png

    // 오픈 안했을때 : 무조건 cpu.png
    String tempImgSrc = '';
    if (_isOpen) {
      if (widget.pandon < widget.currentMoney) {
        tempImgSrc = 'assets/Exc.png';
      } else if (widget.pandon == widget.currentMoney) {
        tempImgSrc = 'assets/one.png';
      } else {
        tempImgSrc = 'assets/two.png';
      }
    } else {
      tempImgSrc = 'assets/cup.png';
    }

    setState(() {
      imgSrc = tempImgSrc;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _alsdkfj();
        _calculateImgSrc();
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: SizedBox(
          width: 135,
          height: 150,
          child: Image.asset(imgSrc),
        ),
      ),
    );
  }
}
